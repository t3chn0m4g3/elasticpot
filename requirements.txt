configparser>=3.5.0
service_identity>=18.1.0
setuptools<45.0.0
Twisted>=18.9.0		# If this fails, try sudo apt-get install python-twisted
geoip2>=2.7.0
maxminddb>=1.3.0

# The following modules are required for the corresponding output plugins

# MySQL
# If this fails, see the documentation docs/sql/README.md
#  or try `sudo apt-get install libmysqlclient-dev`
mysqlclient>=1.3.12

# hpfeed
hpfeeds>=3.0.0

# influx
influxdb

# influx2 (requires Python 3)
#influxdb-client

# mongodb
pymongo

# redisdb
redis

# rethinkdblog
rethinkdb>=2.4

# couchdb
couchdb

# elasticlog
elasticsearch>=7.7.1


# TODO:

# kafka
#afkak
#python-snappy

